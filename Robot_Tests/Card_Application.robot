*** Settings ***
Documentation     A test suite for the Card Application project. The application is tested in an end-to-end manner.
...               All users are cleared from the database at the start of each test.
Library           OperatingSystem
Library           Process
Library           String

*** Test Cases ***
1_createPerson_201Create
    [Documentation]    Create a new valid Person.
    [Setup]    Clear Database
    ${response}=    Run Process    curl -ksi --request POST --url http://localhost:8080/users --header "Content-Type: application/json" --data "{ \\"personPIN\\":\\"12345678910\\",\\"firstName\\":\\"Andrej\\",\\"lastName\\":\\"Smith\\",\\"cardStatus\\":\\"UNAPPLIED\\" }"    shell=yes
    Log    ${response.stdout}
    Should Contain    ${response.stdout}    201
    Should Contain    ${response.stdout}    personPIN
    Should Contain    ${response.stdout}    firstName
    Should Contain    ${response.stdout}    lastName
    Should Contain    ${response.stdout}    cardStatus

2_createSamePersonTwice_302Found
    [Documentation]    Create a new valid Person, then try to create him again. 302 Found should be returned for the second request.
    [Setup]    Clear Database
    ${response}=    Run Process    curl -ksi --request POST --url http://localhost:8080/users --header "Content-Type: application/json" --data "{ \\"personPIN\\":\\"12345678910\\",\\"firstName\\":\\"Andrej\\",\\"lastName\\":\\"Smith\\",\\"cardStatus\\":\\"UNAPPLIED\\" }"    shell=yes
    Log    ${response.stdout}
    Should Contain    ${response.stdout}    201
    Should Contain    ${response.stdout}    personPIN
    Should Contain    ${response.stdout}    firstName
    Should Contain    ${response.stdout}    lastName
    Should Contain    ${response.stdout}    cardStatus
    #try to create user with the same PIN
    ${response}=    Run Process    curl -ksi --request POST --url http://localhost:8080/users --header "Content-Type: application/json" --data "{ \\"personPIN\\":\\"12345678910\\",\\"firstName\\":\\"Andrej\\",\\"lastName\\":\\"Smith\\",\\"cardStatus\\":\\"UNAPPLIED\\" }"    shell=yes
    Log    ${response.stdout}
    Should Contain    ${response.stdout}    302

3_createPerson_deletePerson_getPerson_404NotFound
    [Documentation]    Create a new valid Person, check that he was sucessfully created (GET request). Afterwards, delete the user via his PIN, then try to fetch him again, expecting a 404 Not Found Response.
    [Setup]    Clear Database
    ${response}=    Run Process    curl -ksi --request POST --url http://localhost:8080/users --header "Content-Type: application/json" --data "{ \\"personPIN\\":\\"12345678910\\",\\"firstName\\":\\"Andrej\\",\\"lastName\\":\\"Smith\\",\\"cardStatus\\":\\"UNAPPLIED\\" }"    shell=yes
    Log    ${response.stdout}
    Should Contain    ${response.stdout}    201
    Should Contain    ${response.stdout}    personPIN
    Should Contain    ${response.stdout}    firstName
    Should Contain    ${response.stdout}    lastName
    Should Contain    ${response.stdout}    cardStatus
    #get user
    ${response}=    Run Process    curl -ksi --request GET --url http://localhost:8080/users/12345678910    shell=yes
    Log    ${response.stdout}
    Should Contain    ${response.stdout}    200
    #delete user
    ${response}=    Run Process    curl -ksi --request DELETE --url http://localhost:8080/users/12345678910    shell=yes
    Log    ${response.stdout}
    Should Contain    ${response.stdout}    204
    #get again (not found)
    ${response}=    Run Process    curl -ksi --request GET --url http://localhost:8080/users/12345678910    shell=yes
    Log    ${response.stdout}
    Should Contain    ${response.stdout}    404

4_applyForCard_deleteUser_fileMarkedAsInactive
    [Documentation]    Create a new valid Person, send a request to apply that person for a card, check that file was successfully created in the working directory. Afterward, delete the user and check that the file was marked as inactive (card status set to UNAPPLIED)
    [Setup]    Clear Database
    ${response}=    Run Process    curl -ksi --request POST --url http://localhost:8080/users --header "Content-Type: application/json" --data "{ \\"personPIN\\":\\"12345678910\\",\\"firstName\\":\\"Andrej\\",\\"lastName\\":\\"Smith\\",\\"cardStatus\\":\\"UNAPPLIED\\" }"    shell=yes
    Log    ${response.stdout}
    Should Contain    ${response.stdout}    201
    Should Contain    ${response.stdout}    personPIN
    Should Contain    ${response.stdout}    firstName
    Should Contain    ${response.stdout}    lastName
    Should Contain    ${response.stdout}    cardStatus
    #apply for card
    ${response}=    Run Process    curl -ksi --request PUT --url http://localhost:8080/users/12345678910/apply    shell=yes
    Log    ${response.stdout}
    Should Contain    ${response.stdout}    200
    #check card application file
    ${file}=    Get File    ..${/}target${/}card_applicant_files${/}12345678910.txt
    Should Contain    ${file}    12345678910,Andrej,Smith,APPLIED
    #delete user
    ${response}=    Run Process    curl -ksi --request DELETE --url http://localhost:8080/users/12345678910    shell=yes
    Log    ${response.stdout}
    Should Contain    ${response.stdout}    204
    #get again (not found)
    ${response}=    Run Process    curl -ksi --request GET --url http://localhost:8080/users/12345678910    shell=yes
    Log    ${response.stdout}
    Should Contain    ${response.stdout}    404
    #check card status in file (UNAPPLIED)
    ${file}=    Get File    ..${/}target${/}card_applicant_files${/}12345678910.txt
    Should Contain    ${file}    12345678910,Andrej,Smith,UNAPPLIED

*** Keywords ***
Clear Database
    [Documentation]    Deletes all orders, pancakes and ingredients
    #delete users
    ${response}=    Run Process    curl -ksi --request DELETE --url http://localhost:8080/users    shell=yes
    Should Contain    ${response.stdout}    204
