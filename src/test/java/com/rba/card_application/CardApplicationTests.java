package com.rba.card_application;

import com.rba.card_application.helpers.FileHelper;
import com.rba.card_application.model.CardStatus;
import com.rba.card_application.model.Person;
import com.rba.card_application.repositories.PersonRepository;
import com.rba.card_application.services.PersonService;
import org.h2.engine.User;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@SpringBootTest
class CardApplicationTests {

	@Autowired
	PersonRepository personRepository;

	@Autowired
	private PersonService personService;

	Person person;

	String personalPIN = "12345678910";

	@BeforeEach
	public void preparePerson(){
		person = new Person();
		person.setPersonPIN(personalPIN);
		person.setFirstName("Andrej");
		person.setLastName("Smith");
		person.setCardStatus(CardStatus.UNAPPLIED);
	}

	@Test
	@Transactional
	void addPerson_getPerson_deletePerson() {
		personRepository.save(person);

		Person personFromDb = personRepository.getPersonByPersonPIN(personalPIN);
		assert person.equals(personFromDb);

		personRepository.deleteAll();
		personFromDb = personRepository.getPersonByPersonPIN(personalPIN);
		assert personFromDb == null;
	}

	@Test
	@Transactional
	void applicationFileCreated_thenUserDeleted() throws IOException {
		personRepository.save(person);
		personService.createCardApplication(person);
		Person personFromDb = personRepository.getPersonByPersonPIN(personalPIN);
		assert person.equals(personFromDb);

		String fileContent = Files.readString(Path.of(FileHelper.directoryLocation + "/" + personalPIN + ".txt"));
		assert "12345678910,Andrej,Smith,APPLIED".equals(fileContent); //check that file was created with the expected content

		personService.deletePersonByPIN(personalPIN);
		personFromDb = personRepository.getPersonByPersonPIN(personalPIN);
		assert personFromDb == null; //person deleted from database

		fileContent = Files.readString(Path.of("./card_applicant_files/" + personalPIN + ".txt"));
		assert "12345678910,Andrej,Smith,UNAPPLIED".equals(fileContent); //application file status changed to UNAPPLIED
	}

	@AfterAll
	public static void clean() {
		File dir = new File(FileHelper.directoryLocation);
		for (File file:dir.listFiles()) {
			file.delete();
		}
		dir.delete();
	}
}
