package com.rba.card_application.repositories;

import com.rba.card_application.model.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {
    Person getPersonByPersonPIN(String personPIN);

    void deleteByPersonPIN(String personPIN);
}
