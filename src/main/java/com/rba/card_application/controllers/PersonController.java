package com.rba.card_application.controllers;

import com.rba.card_application.helpers.FileHelper;
import com.rba.card_application.model.CardStatus;
import com.rba.card_application.model.Person;
import com.rba.card_application.services.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for people applying for a bank card. People are provisioned independently of the card application process.
 * POST /users - create a new user
 * GET /users - fetch a list of all users
 * GET /users/{PIN} - get user specified by his personal PIN
 * DELETE /users - delete all users
 * DELETE /users/{PIN} - delete user specified by his personal PIN
 * PUT /users/{PIN}/apply - create a card application file for the specific user, and set his card status to {@link CardStatus#APPLIED}
 */

@RestController
public class PersonController {

    private static final Logger logger = LoggerFactory.getLogger("PersonLogger");

    @Autowired
    private PersonService personService;

    // Save operation
    @PostMapping("/users")
    public ResponseEntity<Person>  savePerson(@Valid @RequestBody Person person)
    {
        String personPIN = person.getPersonPIN();
        Optional<Person> existingPerson = personService.getPerson(personPIN);
        logger.info("Received a new request to save person with PIN " + personPIN);
        if(existingPerson.isPresent()){
            logger.info("Person with PIN " + personPIN + " already exists");
            return new ResponseEntity<>(existingPerson.get(), HttpStatus.FOUND); //person with this unique PIN already exists
        }
        person.setCardStatus(CardStatus.UNAPPLIED); //user is about to be created, he has not yet applied
        return new ResponseEntity<>(personService.savePerson(person), HttpStatus.CREATED);
    }

    // Read operation
    @GetMapping("/users")
    public ResponseEntity<List<Person>> getPersonList()
    {
        List<Person> people = personService.getPersonList();
        if(people == null || people.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        else return new ResponseEntity<>(people, HttpStatus.OK);
    }

    // Read operation
    @GetMapping("/users/{PIN}")
    public ResponseEntity<Person> getPerson(@PathVariable String PIN)
    {
        Optional<Person> optionalPerson = personService.getPerson(PIN);
        if (!optionalPerson.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(optionalPerson.get());
    }


    // Delete operation
    @DeleteMapping("/users")
    public ResponseEntity<Object> deleteAllUsers()
    {
        personService.deleteAllPeople();
        logger.info("All user data deleted");
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // Delete operation
    @DeleteMapping("/users/{PIN}")
    public ResponseEntity<Object> deleteUserByPIN(@PathVariable String PIN)
    {
        personService.deletePersonByPIN(PIN);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/users/{PIN}/apply")
    public ResponseEntity<Object> createApplication(@PathVariable String PIN) {
        Optional<Person> optionalPerson = personService.getPerson(PIN);
        if (!optionalPerson.isPresent()) {
            return new ResponseEntity<>("Person specified by PIN not found", HttpStatus.NOT_FOUND);
        }

        Person applicant = optionalPerson.get();
        boolean already_applied = FileHelper.checkIfApplied(applicant.getPersonPIN());
        if(already_applied) return new ResponseEntity<>("The requested person has already applied for a card.", HttpStatus.FOUND);

        boolean success = personService.createCardApplication(applicant);
        if(success) return new ResponseEntity<>("Successfully created card application file.", HttpStatus.OK);
        else return new ResponseEntity<>("Unable to create card application.", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
