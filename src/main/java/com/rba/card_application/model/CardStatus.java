package com.rba.card_application.model;

public enum CardStatus {
    UNAPPLIED, APPLIED
}
