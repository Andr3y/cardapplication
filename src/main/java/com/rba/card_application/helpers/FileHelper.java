package com.rba.card_application.helpers;

import com.rba.card_application.model.CardStatus;
import com.rba.card_application.model.Person;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Helper class with static methods for file handling procedures concerning the card application files.
 **/

public class FileHelper {
    public static final String directoryLocation = "./card_applicant_files";

    private static final String FILE_DELIMITER = ",";

    /**
     * Checks whether the directory used for housing the card application files exists.
     * If it does not, the directory is created.
     * The location of the directory is taken from a constant static variable in the FileHelper class.
     * @return Returns true if the directory already exists or was successfully created; false in case it does not exist
     * and the method was unable to create the directory
     */
    public static boolean checkOrCreateDirectory(){
        File directory = new File(directoryLocation);
        if(!directory.exists()){
            return directory.mkdir(); //returns false if directory can't be created
        }
        return true; //directory already exists, do nothing
    }

    /**
     * Creates a card application file for the given applicant.
     * The file is created in the directory specified by the FileHelper class.
     * Personal data of the applicant joined by a delimiter specified by the FileHelper class is written into the file.
     * The card status of the applicant is always set to APPLIED upon creation.
     * @param applicant for whom the file will be created.
     * @return Returns true if the file was created successfully, false otherwise.
     */
    public static boolean createApplicationFile(Person applicant) {
        try {
            String applicationFilePath = getApplicationFilename(applicant.getPersonPIN());
            FileWriter fileWriter = new FileWriter(applicationFilePath, false);
            fileWriter.append(String.join(FILE_DELIMITER, applicant.getPersonPIN(), applicant.getFirstName(), applicant.getLastName(), CardStatus.APPLIED.toString()));
            fileWriter.flush();
            fileWriter.close();
            return true;
        }catch (IOException ioex){
            return false;
        }
    }


    /**
     * Checks whether a card application file already exists for a person with the provided personal PIN.
     * @param personPIN of the applicant in question
     * @return Returns true if the file already exist and the card status in the file equals {@link CardStatus#APPLIED}.
       Otherwise, returns false (file does not exist or is inactive - {@link CardStatus#UNAPPLIED} status).
     */
    public static boolean checkIfApplied(String personPIN){
        String applicationFilePath= getApplicationFilename(personPIN);
        File applicationFile = new File(applicationFilePath);
        if(!applicationFile.exists()) return false;
        try {
            String fileContent = Files.readString(Path.of(applicationFilePath));
            if(fileContent != null && fileContent.contains(FILE_DELIMITER + CardStatus.APPLIED.toString())) return true; //person already has an application for a card
        } catch (IOException e) {
            return false; //assume person has not applied if we're unable to read the file
        }
        return false;
    }

    /**
     * Marks a card application file as inactive (card status in file is set to {@link CardStatus#UNAPPLIED}).
     * @param personPIN of the applicant in question
     * @return Returns true if the file was successfully marked as inactive, i.e the card status was successfully changed to {@link CardStatus#UNAPPLIED}
       Otherwise, returns false.
     */
    public static boolean markFileAsInactive(String personPIN) {
        String applicationFilePath= getApplicationFilename(personPIN);
        try {
            String fileContent = new String(Files.readString(Path.of(applicationFilePath)));
            if(fileContent != null) fileContent = fileContent.replace(CardStatus.APPLIED.toString(), CardStatus.UNAPPLIED.toString());
            FileWriter fileWriter = new FileWriter(applicationFilePath, false);
            fileWriter.append(fileContent);
            fileWriter.flush();
            fileWriter.close();
            return true;
        } catch (IOException e) {
            return false; //continue even if we're unable to replace the Card Status in the application file
        }
    }

    private static String getApplicationFilename(String personPIN){
        checkOrCreateDirectory(); //check whether working directory for card application files already exists, if not create it
        return directoryLocation + "/" + personPIN + ".txt";
    }
}
