package com.rba.card_application.services;

import com.rba.card_application.helpers.FileHelper;
import com.rba.card_application.model.CardStatus;
import com.rba.card_application.model.Person;
import com.rba.card_application.repositories.PersonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.File;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PersonServiceImpl implements PersonService {

    private static final Logger logger = LoggerFactory.getLogger("PersonLogger");

    @Autowired
    private PersonRepository personRepository;

    @Override
    public Person savePerson(Person person) {
        return personRepository.save(person);
    }

    @Override
    public List<Person> getPersonList() {
        return (List<Person>) personRepository.findAll();
    }

    @Override
    public Optional<Person> getPerson(String personPIN) {
        return Optional.ofNullable(personRepository.getPersonByPersonPIN(personPIN));
    }

    @Override
    public void deleteAllPeople() {
        List<Person> people = (List<Person>) personRepository.findAll(); // get all people in database
        for(Person person : people){
            FileHelper.markFileAsInactive(person.getPersonPIN()); //mark each file as inactive before deletion from DB
        }
        personRepository.deleteAll();
    }

    @Override
    public void deletePersonByPIN(String personPIN) {
        personRepository.deleteByPersonPIN(personPIN);
        logger.info("Deleted person with PIN " + personPIN);
        if(FileHelper.checkIfApplied(personPIN)) {
           boolean success =  FileHelper.markFileAsInactive(personPIN);
           if(!success) logger.warn("Unable to mark card application file as inactive for person with PIN " + personPIN);
        }
    }

    public boolean createCardApplication(Person applicant){
        boolean success = FileHelper.createApplicationFile(applicant);
        if(success){ //update person status in database, APPLIED
            applicant.setCardStatus(CardStatus.APPLIED);
            personRepository.save(applicant);
            logger.info("Created card application for applicant with PIN " + applicant.getPersonPIN());
        } else logger.error("Unable to create card application file for person with PIN " + applicant.getPersonPIN());

        return success;
    }

}
