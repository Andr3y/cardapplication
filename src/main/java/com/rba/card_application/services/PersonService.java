package com.rba.card_application.services;

import com.rba.card_application.model.Person;

import java.util.List;
import java.util.Optional;

public interface PersonService {

    // Save operation
    Person savePerson(Person person);

    // Read operation
    List<Person> getPersonList();

    Optional<Person> getPerson(String personPIN);

    void deleteAllPeople();

    void deletePersonByPIN(String personPIN);

    boolean createCardApplication(Person applicant);

}
