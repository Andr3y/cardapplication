The application simulates a REST API interface used by bank clients that want to apply for a new card. 
A user with a PIN, first name, last name and status can be created. 
Afterwards, the user can apply for a card. The application then creates a file in the working directory, containing the user data,
with the assumption that a card generation process will read the files.
The card generation process is outside the scope of this project, and the generated files are only used as a proof-of-concept. 

When the user is first created, his card status is automatically set to UNAPPLIED. Once the user sends a request to apply for a card, 
his status is updated to APPLIED, and a corresponding card application file is generated.
If the user is deleted, rather than deleting the application file, the status inside of it will be set to UNAPPLIED, marking it 
as inactive. A user corresponds to exactly one file via his unique PIN.

The following REST endpoints can be used:

* POST /users - create a new user
* GET /users - fetch a list of all users
* GET /users/{PIN} - get user specified by his personal PIN
* DELETE /users - delete all users
* DELETE /users/{PIN} - delete user specified by his personal PIN
* PUT /users/{PIN}/apply - apply a specific user for a card


Alongside JUnit tests, end-to-end Robot tests have been created to externally test the application functionality. Python with the robotframework library is required to run the test suite. A log file of the last successful test execution is contained within the root project directory, as proof of a successful test run.